/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.fxworkbench.fxwindow.fxmlcontroller.impl;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.FXMLController;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.FXMLControllerServices;
import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.ProvidedService;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.core.logging.LoggingServices;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@ComponentImplementation
public abstract class FXMLControllerImpl implements FXMLController, FXMLControllerServices {

    @ProvidedService
    public final transient FXMLControllerServices fxmlControllerServices = this;

    @UsedService
    protected final transient LoggingServices loggingServices = null;

    @Override
    public Parent loadFXML() {
        Parent root = null;

        URL fxmlUrl = this.getFXMLUrl();
        if (fxmlUrl != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl);
            fxmlLoader.setController(this.getFXControllerBean());
            try {
                root = fxmlLoader.load();
                this.onFXMLLoaded();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return root;
    }

    @Override
    public void dispose() {

    }

    abstract public URL getFXMLUrl();

    abstract public Object getFXControllerBean();

    abstract public void onFXMLLoaded();
}
