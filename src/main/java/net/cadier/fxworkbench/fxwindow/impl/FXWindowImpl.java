/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.fxworkbench.fxwindow.impl;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import net.cadier.fxworkbench.fxutils.FXUtils;
import net.cadier.fxworkbench.fxwindow.FXWindow;
import net.cadier.fxworkbench.fxwindow.FXWindowEvent;
import net.cadier.fxworkbench.fxwindow.FXWindowServices;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.FXMLController;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.FXMLControllerServices;
import net.cadier.jcf.annotations.*;
import net.cadier.jcf.core.container.ContainerServices;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.exception.MalformedComponentException;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.Source;
import net.cadier.utils.reflection.ReflectionException;

import java.util.concurrent.atomic.AtomicBoolean;

@ComponentImplementation
public class FXWindowImpl implements FXWindow {
    @Id
    private final transient InstanceId windowId = null;

    @ProvidedService
    public final transient FXWindowServices fxWindowServices = new FXWindowServicesImpl();

    @UsedService
    private final transient ContainerServices containerServices = null;

    @UsedService
    private final transient FXMLControllerServices fxmlControllerService = null;

    @EventSource
    public final transient Source<FXWindowEvent> fxWindowEventEventsSource = null;

    private class FXWindowServicesImpl implements FXWindowServices {
        private Stage primaryStage;
        private Class<?> fxmlControllerClass;
        private Class<?> modelClass;

        private final AtomicBoolean active = new AtomicBoolean(false);

        @Override
        public void setController(final Class<? extends FXMLController> aFXMLControllerClass) throws MalformedComponentException {
            this.fxmlControllerClass = aFXMLControllerClass;

            // register FXML controller
            containerServices.registerComponent(this.fxmlControllerClass);
        }

        @Override
        public void setModel(final Class<?> aModelClass) throws MalformedComponentException {
            this.modelClass = aModelClass;

            // register FXML model
            containerServices.registerComponent(this.modelClass);
        }

        @Override
        public void buildWindow(final String title) throws ReflectionException, DeploymentRuleException, DependencyResolutionException {
            // deploy controller under window
            containerServices.deployInstance(windowId, fxmlControllerClass, true);

            // eventually start FX Toolkit
            FXUtils.startToolkit(() -> {
                fxWindowEventEventsSource.postEvent(new FXWindowEvent(null, FXWindowEvent.FXWindowEventType.LAST_WINDOW_CLOSED));
            });

            // build stage
            Parent root = fxmlControllerService.loadFXML();
            FXUtils.runAndWait(() -> { // TODO pq runLater ne suffit pas ??
                primaryStage = new Stage();
                primaryStage.setScene(new Scene(root));
                primaryStage.setTitle(title);

                primaryStage.setOnCloseRequest(event -> {
                    if (WindowEvent.WINDOW_CLOSE_REQUEST == event.getEventType()) {
                        fxWindowEventEventsSource.postEvent(new FXWindowEvent(windowId, FXWindowEvent.FXWindowEventType.WINDOW_CLOSE_REQUEST));
                    }
                });
            });

            // deploy model under window (after the window has been built to ensure that @FXML entry points have been injected)
            containerServices.deployInstance(windowId, modelClass, true);
        }

        @Override
        public void showWindow() {
            // show stage
            FXUtils.runLater(primaryStage::show);
        }

        @Override
        public void focusOnWindow() {
            FXUtils.runLater(() -> {
                primaryStage.requestFocus();
                primaryStage.getScene().getRoot().requestFocus();
            });
        }

        @Override
        public void closeWindow() {
            FXUtils.runLater(primaryStage::close);
        }
    }
}

