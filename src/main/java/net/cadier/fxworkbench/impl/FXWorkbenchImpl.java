/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.fxworkbench.impl;

import net.cadier.fxworkbench.FXWorkbench;
import net.cadier.fxworkbench.FXWorkbenchServices;
import net.cadier.fxworkbench.fxutils.FXUtils;
import net.cadier.fxworkbench.fxwindow.FXWindowEvent;
import net.cadier.fxworkbench.fxwindow.FXWindowServices;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.FXMLController;
import net.cadier.fxworkbench.fxwindow.impl.FXWindowImpl;
import net.cadier.jcf.annotations.*;
import net.cadier.jcf.core.container.ContainerServices;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.exception.MalformedComponentException;
import net.cadier.jcf.lang.component.Startable;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.Sink;
import net.cadier.utils.reflection.ReflectionException;

import java.util.Map;

@ComponentImplementation
public class FXWorkbenchImpl implements FXWorkbench, Startable {
    @Id
    private final transient InstanceId thisId = null;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @UsedService
    private final transient ContainerServices containerServices = null;

    @UsedService
    private final transient Map<InstanceId, FXWindowServices> fxWindowServices = null;

    @EventSink
    private final transient Sink<FXWindowEvent> fxWindowEventEventsSink = this::processEvent;

    private void processEvent(FXWindowEvent fxWindowEvent) {
        InstanceId windowId = fxWindowEvent.getWindowId();
        FXWindowEvent.FXWindowEventType eventType = fxWindowEvent.getEventType();

        switch (eventType) {
            case WINDOW_CLOSE_REQUEST:
                // undeploy FXWindowImpl instance
                containerServices.undeployInstance(windowId);
                break;
            case LAST_WINDOW_CLOSED:
                // as we didn't start JavaFX through Application, we have to explicitly stop the toolkit
                FXUtils.stopToolkit();

                // unregister FXWindowImpl
                containerServices.unregisterComponent(FXWindowImpl.class);

                // terminate jCF container
                containerServices.terminate(false);
                break;
        }
    }

    @ProvidedService
    public final transient FXWorkbenchServices FXWorkbenchServices = new FXWorkbenchServicesImpl();

    @Override
    public boolean onStarted() {
        // register FXWindow for use in service
        try {
            this.containerServices.registerComponent(FXWindowImpl.class);
            return true;
        } catch (MalformedComponentException e) {
            // should not happen since FXWindowImpl has been extensively tested
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean onPaused() {
        return true;
    }

    @Override
    public boolean onResumed() {
        return true;
    }

    @Override
    public boolean onStopped() {
        if (fxWindowServices != null) {
            for (InstanceId fxWindowId : fxWindowServices.keySet()) {
                fxWindowServices.get(fxWindowId).closeWindow();
            }
        }
        return true;
    }

    @Override
    public boolean onFailed() {
        return false;
    }

    private class FXWorkbenchServicesImpl implements FXWorkbenchServices {

        @Override
        public InstanceId newWindow(final Class<? extends FXMLController> fxmlControllerClass,
                                    final Class<?> modelClass) {
            // deploy a new FXWindow instance
            InstanceId fxWindowId = null;
            try {
                fxWindowId = containerServices.deployInstance(thisId, FXWindowImpl.class, true);
            } catch (DeploymentRuleException | DependencyResolutionException | ReflectionException e) {
                e.printStackTrace();
            }
            FXWindowServices fxWindowServices = FXWorkbenchImpl.this.fxWindowServices.get(fxWindowId);

            // set FXWindow controller and model
            try {
                fxWindowServices.setController(fxmlControllerClass);
                fxWindowServices.setModel(modelClass);
            } catch (MalformedComponentException mce) {
                mce.printStackTrace();
            }

            return fxWindowId;
        }

        @Override
        public void buildWindow(final InstanceId fxWindowId, final String title) {
            FXWindowServices fxWindowServices = FXWorkbenchImpl.this.fxWindowServices.get(fxWindowId);
            try {
                fxWindowServices.buildWindow(title);
            } catch (DeploymentRuleException | ReflectionException e) {
                loggingServices.error("Looks like either the FXML controller or model class has not been registered before building the window. Check that setController(...) and setModel(...) are called before buildWindow(...).");
                e.printStackTrace();
                System.exit(-1);
            } catch(DependencyResolutionException e) {
                loggingServices.error("Looks like either the FXML controller or model class is not properly defined. Checked the following stacktrace:");
                e.printStackTrace();
                System.exit(-1);
            }
            fxWindowServices.showWindow();
        }

        @Override
        public void focusOnWindow(final InstanceId fxWindowId) {
            FXWindowServices fxWindowServices = FXWorkbenchImpl.this.fxWindowServices.get(fxWindowId);
            fxWindowServices.focusOnWindow();
        }

        @Override
        public void closeWindow(final InstanceId fxWindowId) {
            FXWindowServices fxWindowServices = FXWorkbenchImpl.this.fxWindowServices.get(fxWindowId);
            fxWindowServices.closeWindow();
        }
    }
}

