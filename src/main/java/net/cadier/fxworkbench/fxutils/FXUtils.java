/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.fxworkbench.fxutils;

import com.sun.javafx.application.PlatformImpl;
import javafx.application.Platform;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

public final class FXUtils {

    /**
     * Flags the Initialized state of the JFX Toolkit.
     */
    private static final AtomicBoolean ToolkitInitialized = new AtomicBoolean(false);

    /**
     * Start Toolkit if it's not already running.
     *
     * @param idlePlatformListener  runnable started when idle is detected
     */
    public static void startToolkit(final Runnable idlePlatformListener) {
        if (!FXUtils.ToolkitInitialized.getAndSet(true)) {
            CountDownLatch cdl = new CountDownLatch(1);

            // initialize JFX Toolkit
            Platform.startup(cdl::countDown);

            // implicit exit and listener to be notified when javafx goes idle
            Platform.setImplicitExit(true);
            PlatformImpl.addListener(new PlatformImpl.FinishListener() {
                @Override
                public void idle(boolean implicitExit) {
                    idlePlatformListener.run();
                }

                @Override
                public void exitCalled() {
                    // nothing to do
                }
            });

            try {
                cdl.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void stopToolkit() {
        Platform.exit();
    }

    /**
     * Execute runnable in FxApplicationThread.
     *
     * @param runnable runnable to execute.
     */
    public static void runLater(Runnable runnable) {
        // starts Toolkit if needed
        FXUtils.startToolkit(() -> {});

        Platform.runLater(runnable);
    }

    /**
     * Execute runnable in FxApplicationThread and wait for termination.
     *
     * @param runnable runnable to execute.
     */
    public static void runAndWait(Runnable runnable) {
        // starts Toolkit if needed
        FXUtils.startToolkit(() -> {});

        final CountDownLatch doneLatch = new CountDownLatch(1);
        Platform.runLater(() -> {
            try {
                runnable.run();
            } finally {
                doneLatch.countDown();
            }
        });

        try {
            doneLatch.await();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }


}
