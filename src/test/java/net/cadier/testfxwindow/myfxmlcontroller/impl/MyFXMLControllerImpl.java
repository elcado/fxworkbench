/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.testfxwindow.myfxmlcontroller.impl;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import net.cadier.fxworkbench.fxutils.FXUtils;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.impl.FXMLControllerImpl;
import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSink;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.lang.instance.events.Sink;
import net.cadier.testfxwindow.myfxmlcontroller.MyFXMLController;
import net.cadier.testfxwindow.myfxmodel.MyFXModelEvent;
import net.cadier.testfxwindow.myfxmodel.MyFXModelServices;

import java.net.URL;

@ComponentImplementation
public class MyFXMLControllerImpl extends FXMLControllerImpl implements MyFXMLController {
    @FXML
    private Button aButton;

    @FXML
    private Label aLabel;

    @UsedService
    protected final transient MyFXModelServices fxModelServices = null;

    @EventSink
    private final transient Sink<MyFXModelEvent> fxModelEventsSink = this::processEvent;

    @Override
    public URL getFXMLUrl() {
        return AnotherMyFXMLControllerImpl.class.getClassLoader().getResource("test.fxml");
    }

    @Override
    public Object getFXControllerBean() {
        return this;
    }

    @Override
    public void onFXMLLoaded() {
        aButton.setOnAction(event -> {
            fxModelServices.countUp();
        });
    }

    public void processEvent(MyFXModelEvent myFXModelEvent) {
        switch (myFXModelEvent.getEventType()) {
            case COUNT_UPDATED:
                FXUtils.runLater(() -> {
                    aLabel.setText("Button has been clicked " + fxModelServices.getCount());
                });
                break;
        }
    }
}
