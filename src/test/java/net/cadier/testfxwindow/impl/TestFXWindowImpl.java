/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.testfxwindow.impl;

import javafx.scene.Node;
import javafx.scene.control.Label;
import net.cadier.fxworkbench.FXWorkbenchServices;
import net.cadier.fxworkbench.impl.FXWorkbenchImpl;
import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.Id;
import net.cadier.jcf.annotations.Parameter;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.core.container.ContainerServices;
import net.cadier.jcf.core.container.impl.ContainerImpl;
import net.cadier.jcf.core.logging.LoggingParameters;
import net.cadier.jcf.core.logging.LoggingServices;
import net.cadier.jcf.exception.DependencyResolutionException;
import net.cadier.jcf.exception.DeploymentRuleException;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.parameters.Parameters;
import net.cadier.testfxwindow.TestFXWindow;
import net.cadier.testfxwindow.myfxmlcontroller.impl.AnotherMyFXMLControllerImpl;
import net.cadier.testfxwindow.myfxmlcontroller.impl.MyFXMLControllerImpl;
import net.cadier.testfxwindow.myfxmodel.impl.MyFXModelImpl;
import net.cadier.utils.reflection.ReflectionException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.util.WaitForAsyncUtils;

import java.util.Arrays;
import java.util.logging.Level;

@ComponentImplementation
public class TestFXWindowImpl implements TestFXWindow {
    @Id
    private final transient InstanceId thisId = null;

    @UsedService
    private final transient ContainerServices containerServices = null;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @UsedService
    private final transient FXWorkbenchServices fxWorkbenchServices = null;

    @Parameter
    private final transient Parameters<LoggingParameters> loggingParameters = null;

    /**
     * Default jcf logging level to be restored at the end of each test
     */
    private Object defaultLoggingLevel;

    /**
     * TestFX robot
     */
    private final FxRobot fxRobot = new FxRobot();


    @RegisterExtension
    BeforeAllCallback beforeAllCallback = context -> {
        //        if (Boolean.getBoolean("headless")) {
        //            System.setProperty("testfx.robot", "glass");
        //            System.setProperty("testfx.headless", "true");
        //            System.setProperty("prism.order", "sw");
        //            System.setProperty("prism.text", "t2k");
        //            System.setProperty("java.awt.headless", "true");
        //        }
    };


    @RegisterExtension
    BeforeEachCallback beforeEachCallback = context -> {
        // -Dglass.platform=Monocle -Dmonocle.platform=Headless -Dprism.order=sw
        // System.setProperty("testfx.robot", "glass");
        // System.setProperty("testfx.headless", "true");

//        System.setProperty("glass.platform", "Monocle");
//        System.setProperty("monocle.platform", "Headless");
//        System.setProperty("prism.order", "sw");

        // deploy container
        ContainerImpl.initialize(this);

        assert this.thisId != null;

        // log level
        this.defaultLoggingLevel = this.loggingParameters.set(LoggingParameters.LOGGING_LEVEL, Level.ALL);

        this.loggingServices.info("#################################\n" + "### DEPLOYING TEST " + "COMPONENTS ###\n" + "#################################");

        boolean testComponentRegistrationResult = true;
        for (Class<?> testComponent : Arrays.asList(FXWorkbenchImpl.class)) {
            this.containerServices.registerComponent(testComponent);
        }

        if (!testComponentRegistrationResult) {
            System.err.println("\n### ABORTING TEST ###");
            System.exit(-1);
        }

        this.loggingServices.info("######################\n" + "### LAUNCHING TEST ###\n" + "######################");
    };

    @RegisterExtension
    AfterEachCallback afterEachCallback = context -> {
        this.loggingServices.info("###################\n" + "### ENDING TEST ###\n" + "###################");

        this.loggingParameters.set(LoggingParameters.LOGGING_LEVEL, this.defaultLoggingLevel);

        this.containerServices.terminate(true);
    };

    private InstanceId deployInstance(InstanceId parentId, Class<?> componentType, boolean autoStart) {
        this.loggingServices.info("### Deploying " + componentType.getSimpleName() + " instance");
        InstanceId deployedInstanceId = null;
        try {
            deployedInstanceId = this.containerServices.deployInstance(parentId, componentType, autoStart);
        } catch (DeploymentRuleException | DependencyResolutionException | ReflectionException e) {
            e.printStackTrace();
        }
        return deployedInstanceId;
    }

    @Test
    public void testFXWindowDeployment() {
        InstanceId fxWorkbenchId = this.deployInstance(this.thisId, FXWorkbenchImpl.class, true);
        assert fxWorkbenchId != null;

        String window1Title = "Window 1";
        String window2Title = "Window 2";

        InstanceId fxWindowId = this.fxWorkbenchServices.newWindow(MyFXMLControllerImpl.class, MyFXModelImpl.class);
        InstanceId fxWindowId2 = this.fxWorkbenchServices.newWindow(AnotherMyFXMLControllerImpl.class, MyFXModelImpl.class);

        this.fxWorkbenchServices.buildWindow(fxWindowId, window1Title);
        this.fxWorkbenchServices.buildWindow(fxWindowId2, window2Title);

        WaitForAsyncUtils.waitForFxEvents();

        // focus on window 1
        this.fxWorkbenchServices.focusOnWindow(fxWindowId);

        FxAssert.verifyThat("#mainPanel", Node::isVisible);

        // click twice on button
        FxAssert.verifyThat("#aButton", Node::isVisible);
        fxRobot.clickOn("#aButton");
        fxRobot.clickOn("#aButton");

        // check label
        FxAssert.verifyThat("#aLabel", node -> {
            Label label = (Label) node;
            return "Button has been clicked 2".equals(label.getText());
        });

        //waitWorkbench(5000);

        // close fxworkbench
        this.fxWorkbenchServices.closeWindow(fxWindowId);
        this.fxWorkbenchServices.closeWindow(fxWindowId2);
    }
}
